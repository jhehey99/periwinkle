<?php
/**
 * Created by PhpStorm.
 * User: Jhehey
 * Date: 11/16/2018
 * Time: 6:03 AM
 */

namespace api\services\client;

use src\models\BehaviorGraph;
use src\repo\BehaviorRepository;
use src\responses\ApiResponse;
use src\responses\Response;
use src\utils\JsonUtils;

require_once "../../../.autoload.php";

$decoded = JsonUtils::Decode ();

$graph = new BehaviorGraph($decoded);

$bgRepo = new BehaviorRepository();

$response = new ApiResponse();

if($response->IsEmpty ())
{
    if($bgRepo->AddBehaviorGraph ($graph))
        $response->Add (Response::UpdateSuccess ());
    else
        $response->Add (Response::UploadFailed ());
}

$response->Respond ();


