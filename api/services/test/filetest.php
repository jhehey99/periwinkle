<?php
/**
 * Created by PhpStorm.
 * User: Jhehey
 * Date: 11/14/2018
 * Time: 11:38 PM
 */

use src\models\BehaviorGraph;
use src\repo\BehaviorRepository;
use src\responses\ApiResponse;
use src\responses\Response;
use src\utils\JsonUtils;

require_once "../../../.autoload.php";

//$input = file_get_contents('php://input');
//var_dump($input);
//var_dump ($_FILES);
//var_dump ($_POST);

$file = $_FILES['file'];
$obj = $_POST['json'];
$content = file_get_contents($file['tmp_name']);

//var_dump ($file);
//var_dump ($obj);
//print("contents: ");
//var_dump ($content);

$decoded = JsonUtils::DecodeObject ($obj);

$behaviorGraph = new BehaviorGraph($decoded);

//print("behaviorGraph: ");
//var_dump ($behaviorGraph);

//TODO SAVE NA UNG FILE
// SAVE NA UNG OBJECT

$dir = "../../../files/graphs/";
$file = $dir . $behaviorGraph->Filename;

$bgRepo = new BehaviorRepository();

$response = new ApiResponse();

if($response->IsEmpty ())
{
    if($bgRepo->AddBehaviorGraph ($behaviorGraph) && file_put_contents($file, $content))
        $response->Add (Response::UpdateSuccess ());
    else
        $response->Add (Response::UploadFailed ());
}

$response->Respond ();

//TODO PINAKA LATEST NA TODO TO
// ILAGAY MO KO SA services/client
exit();
