<?php
/**
 * Created by PhpStorm.
 * User: Jhehey
 * Date: 11/7/2018
 * Time: 1:05 AM
 */

namespace src\repo;

use src\core\Repository;
use src\models\MbesResponse;

spl_autoload_register(
    function ($class_name)
    {
        require_once $class_name . '.php';
    }
);

/**
 * Class MbesRepository
 * @package src\repo
 */
class MbesRepository extends Repository
{
    public function __construct ()
    {
        parent::__construct ();
    }

    /**
     * @param MbesResponse $response
     * @return bool
     */
    public function InsertMbesResponses(MbesResponse $response) : bool
    {
        $idsLength = count ($response->QuestionIds);
        $valsLength = count($response->ScaleValues);

        if($idsLength != $valsLength)
            return false;

        $length = $idsLength;

        $values = "";

        for($i = 0; $i < $length; $i ++)
        {
            $value = "(" .
                $response->ResponseClientId . "," .
                $response->AttemptId . "," .
                $response->QuestionIds[$i] . "," .
                $response->ScaleValues[$i] .
                "),";

            $values .= $value;
        }

        // remove last ',' in the values string
        $values = rtrim ($values, ",");

        $sql = "INSERT INTO mbesresponse(ResponseClientId, AttemptId, QuestionId, ScaleValue)
            VALUES " . $values;

        $this->prepare ($sql);

        $this->execute ([]);
        return true;
    }

    /**
     * @param string $values
     * @return bool
     */
    public function InsertMbesResponse (string $values) : bool
    {
        $sql = "INSERT INTO mbesresponse(ResponseClientId, AttemptId, QuestionId, ScaleValue)
                VALUES " . $values;

        $this->prepare ($sql);

        return $this->execute ([]);
    }

}

